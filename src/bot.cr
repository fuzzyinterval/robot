# Based on code licensed under the license in the PARSER_LICENSE
# file at the root of the repository.
require "discordcr"
require "./plugin"
require "./plugins/*"
require "yaml"

module Robot
  class Bot
    @client : Discord::Client
    @@instance : Robot::Bot?

    getter config

    def self.instance=(val)
      @@instance = val
    end

    def self.instance
      @@instance
    end

    def initialize(config config_file : String)
      raise "Only one Robot may run at a time" if !@@instance.nil?
      @config = YAML.parse(File.read(File.join(__DIR__, config_file)))
      @client = Discord::Client.new(
        token: "Bot #{@config["bot"]["token"]}",
        client_id: @config["bot"]["client_id"].to_s.to_u64
      )

      @cache = Discord::Cache.new(@client)
      @client.cache = @cache
      @@instance = self if @@instance.nil?
    end

    def run
      Plugin.plugins.each do |plugin|
        plugin.client_init(@client, @cache)
      end

      @client.run
    end
  end
end

Robot::Bot.new(config: "config.yml").run
