module CorePlugin
  class Command
    alias ExecType = Proc(Array(String), Permission, Discord::Message, Discord::Cache, String)
    @exec : ExecType
    @min_args : Int32?
    @max_args : Int32?
    @permission : Permission = Permission::Normal
    property min_args
    property max_args
    property exec
    property permission

    def initialize(@min_args : Int32?, &@exec : ExecType)
    end

    def initialize(&@exec : ExecType)
    end

    def initialize(@min_args : Int32?, @max_args : Int32?, &@exec : ExecType)
    end

    def initialize(@min_args : Int32?, @max_args : Int32?, @permission : Permission, &@exec : ExecType)
    end
  end
end
