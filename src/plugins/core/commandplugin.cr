require "./command"

module CorePlugin
  module CommandPlugin
    @commands : Hash(String, Command)?
    getter commands

    def command(name : String, *args, &block : Array(String), Permission, Discord::Message, Discord::Cache -> _)
      @commands = Hash(String, Command).new if @commands.nil?
      @commands.not_nil![name] = Command.new(*args, &block)
    end

    def client_init(client, cache)
    end
  end
end
