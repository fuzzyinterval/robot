require "./lexer"
require "./error"

module Parser
  record Token,
    type : Symbol, # :COMMAND_START, :COMMAND_END, :NORET_COMMAND_START, :NORET_COMMAND_END, :SEPERATOR, :LITERAL
    content : String

  record Literal,
    content : String

  record Argument,
    contents : Array(Command | Literal)

  record Command,
    name : Argument,
    args : Array(Argument),
    no_return : Bool

  class Parser
    @token : Token?

    def initialize
      @lexer = nil
    end

    def initialize(@lexer : Lexer)
      @token = lexer.next_token
    end

    def reset(lexer : Lexer)
      initialize(lexer)
    end

    def parse : Command
      ensure_lexer
      read_command
    end

    def parse(string) : Command
      reset Lexer.new(string)
      parse
    end

    def ensure_lexer
      raise DeepParserError.new(@lexer.not_nil!, "Attempt to parse without a lexer!") if @lexer.nil?
    end

    def ensure_token
      ensure_lexer
      if @token.nil?
        @token = next_token
        raise DeepParserError.new(@lexer.not_nil!, "Failed getting next token from lexer") if @token.nil?
      end
    end

    private def read_command
      ensure_token

      end_command_token = :INVALID
      end_command_token = :COMMAND_END if @token.not_nil!.type == :COMMAND_START
      end_command_token = :NORET_COMMAND_END if @token.not_nil!.type == :NORET_COMMAND_START

      if end_command_token == :INVALID
        raise DeepParserError.new(@lexer.not_nil!, "Attempt to read command token at a position that doesn't start with a `[` or `{` token")
      end

      if @lexer.not_nil!.finished?
        raise ParserError.new(@lexer.not_nil!, "Missing command ending marker (`#{(end_command_token == :COMMAND_END) ? "]" : "}"}`)")
      end
      next_token

      args = Array(Argument).new

      until @token.not_nil!.type == end_command_token
        args << read_argument
        next_token unless @token.not_nil!.type == end_command_token
      end

      command = args.shift
      Command.new(command, args, no_return: end_command_token == :NORET_COMMAND_END)
    end

    private def read_argument
      ensure_token
      unless {:COMMAND_START, :NORET_COMMAND_START, :LITERAL}.includes? @token.not_nil!.type
        raise DeepParserError.new(@lexer.not_nil!, "Attempt to read argument at a position that doesn't start with a command or literal token")
      end

      contents = Array(Command | Literal).new

      until {:SEPERATOR, :COMMAND_END, :NORET_COMMAND_END}.includes? @token.not_nil!.type
        case @token.not_nil!.type
        when :COMMAND_START
          contents << read_command
        when :NORET_COMMAND_START
          contents << read_command
        when :LITERAL
          contents << read_literal
        end

        next_token
      end

      Argument.new(contents)
    end

    private def read_literal
      ensure_token
      unless @token.not_nil!.type == :LITERAL
        raise DeepParserError.new(@lexer.not_nil!, "Attempt to read literal at a position that doesn't start with a literal token")
      end

      content = @token.not_nil!.content
      Literal.new(content)
    end

    private def next_token
      ensure_lexer
      @token = @lexer.not_nil!.next_token
    end
  end
end

# str = <<-'COMMAND'
#  $ec[$echo h]"o" [$e[$echo c]ho Hello]\ "world"!
# COMMAND
# lexer = Lexer.new(str)
# parser = Parser.new(lexer)
# command = parser.parse
# puts execute_command(command)
