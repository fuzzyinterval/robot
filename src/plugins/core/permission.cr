require "msgpack"

module CorePlugin
  enum Permission
    Normal
    Commander
    Staff
    ServerOwner
    BotOwner
  end
end
